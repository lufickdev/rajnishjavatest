package com.example.rajnish.javatest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
//3
class MobileRam implements Comparator<Mobiles> {

    @Override
    public int compare(Mobiles o1, Mobiles o2) {
        if (o1.ram > o2.ram)
            return -1;
        else
            return 1;
    }
}

class MobilePrice implements Comparator<Mobiles> {

    @Override
    public int compare(Mobiles o1, Mobiles o2) {
        if (o1.price > o2.price)
            return -1;
        else
            return 1;
    }
}

public class MobileArraylist {
    public static void main(String[] args) {
        ArrayList<Mobiles> mobilesArrayList = new ArrayList<>();
        mobilesArrayList.add(new Mobiles("Samsung", "4", "12000"));
        mobilesArrayList.add(new Mobiles("Samsung", "6", "15000"));
        mobilesArrayList.add(new Mobiles("MI", "4", "10000"));
        mobilesArrayList.add(new Mobiles("Mi", "6", "13000"));


        Collections.sort(mobilesArrayList, new MobilePrice());
        for (Mobiles mobiles : mobilesArrayList)
            System.out.println("Name:" + mobiles.price + " " + mobiles.ram + " " + mobiles.modelName);
        Collections.sort(mobilesArrayList,new MobileRam());
        for (Mobiles mobiles:mobilesArrayList)
            System.out.println("Name:"+mobiles.ram+" "+mobiles.modelName);
    }


}


