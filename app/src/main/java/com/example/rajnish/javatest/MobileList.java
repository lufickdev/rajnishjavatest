package com.example.rajnish.javatest;

import java.util.ArrayList;
//4
class MobileA{
    String ModelName;
    String ram;
    String price;
    MobileA(String ModelName,String ram,String price){
        this.ModelName=ModelName;
        this.price=price;
        this.ram=ram;
    }
}

public class MobileList {
    public static void main(String[] args) {
        ArrayList<MobileA> mobiles=new ArrayList<>();
        mobiles.add(new MobileA("Apple","4","5000"));
        mobiles.add(new MobileA("Samsung","6","15000"));
        mobiles.add(new MobileA("Apple","4","50000"));
        mobiles.add(new MobileA("Apple","4","5000"));
        mobiles.add(new MobileA("Mi","6","13000"));
        mobiles.add(new MobileA("Mi","6","13000"));

    }
}
